<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 5/4/14
 * Time: 12:41 PM
 */

require_once('Includes/Stack.class.php');
require_once('Libraries/OptPrecedence.lib.php');

final class MathExpression {
    private $strLastExpression = NULL;
    private $anyResult = NULL;

    public function MathExpression($strExpression=''){
        $this->setExpression($strExpression);
    }
    public function setExpression($strExpression){
        $this->strLastExpression = $strExpression;
        $this->anyResult = NULL;
    }
    public function getResult(){
        if (empty($this->strLastExpression))
            return $this->strLastExpression;
        if ($this->anyResult === NULL){
            $arrStore = $this->splitExpression($this->strLastExpression);
            $arrStore = $this->convertToPostfix($arrStore);
            $this->anyResult = $this->doAction($arrStore);
        }
        return $this->anyResult;
    }
    private function splitExpression($strExpression){
        $lenExpression = strlen($strExpression);
        $index = 0;
        $arrExpression = array();
        $num = '';
        while ($index<$lenExpression){
            $ch = $strExpression[$index];
            if ($ch !== " "){
                if (isOperand($ch))
                    $num .= $ch;
                elseif (($ch == getOperator(DOT_PARENTHESES_OPEN)) || ($ch == getOperator(DOT_PARENTHESES_CLOSE))){
                    if ($num != ''){
                        $arrExpression[] = trim($num);
                        $num = '';
                    }
                    $arrExpression[] = getOperatorName($ch);
                }
                elseif (($ch == getOperator(DOT_OPT_LESS)) || ($ch == getOperator(DOT_OPT_MORE))){
                    if ($num != ''){
                        $arrExpression[] = trim($num);
                        $num = '';
                    }
                    if (($index+1) >= $lenExpression) return array(); //show Error
                    $ch = $strExpression[$index+1];
                    if ($ch == getOperator(DOT_OPT_EQUAL)){
                        $arrExpression[] = getOperatorName($strExpression[$index].$ch);
                        $index++;
                    }
                    else
                        $arrExpression[] = getOperatorName($strExpression[$index]);
                }
                elseif (($ch == getOperator(DOT_OPT_SUB)) || ($ch == getOperator(DOT_OPT_ADD))){
                    if ($num != ''){
                        $arrExpression[] = trim($num);
                    }
                    if ($num == ''){
                        $arrExpression[] = getOperatorName("/".$ch);
                    }
                    else
                        $arrExpression[] = getOperatorName($ch);
                    $num = '';
                }
                else{
                    if ($num != ''){
                        $arrExpression[] = trim($num);
                        $num = '';
                    }
                    $optTemp = getOperatorName($ch);
                    if ($optTemp == NULL) return array();
                    $arrExpression[] = $optTemp ;
                }
            }
            $index++;
        }
        if ($num != '')
            $arrExpression[] = trim($num);
        return $arrExpression;
    }
    
    private function convertToPostfix(array $arrInfix){
        $stackOperator = new stack();
        $arrPostFixExp = array();
        foreach($arrInfix as $symItem){
            if (is_numeric($symItem))
                $arrPostFixExp[] = (double)$symItem;
            elseif ($symItem == getOperatorNameBDOT(DOT_PARENTHESES_OPEN))
                $stackOperator->pushItem($symItem);
            elseif ($symItem == getOperatorNameBDOT(DOT_PARENTHESES_CLOSE)){
                $symOut = $stackOperator->popItem(false);
                while($symOut != getOperatorNameBDOT(DOT_PARENTHESES_OPEN)){
                    $arrPostFixExp[] = $stackOperator->popItem();
                    $symOut = $stackOperator->popItem(false);
                }
                $stackOperator->popItem();
            }
            else{
                $symItemPrec = (int)getPrecedence($symItem);
                $symLastOnStack =  (int)getPrecedence($stackOperator->popItem(false));
                if (($stackOperator->isEmpty()) || ($symItemPrec>$symLastOnStack))
                    $stackOperator->pushItem($symItem);
                else{
                    while ($symItemPrec <= $symLastOnStack){
                        $arrPostFixExp[] = $stackOperator->popItem();
                        $symLastOnStack =  (int)getPrecedence($stackOperator->popItem(false));
                    }
                    $stackOperator->pushItem($symItem);
                }
            }
        }
        while (!$stackOperator->isEmpty())
            $arrPostFixExp[] = $stackOperator->popItem();
        return $arrPostFixExp;
    }


    private function doAction(array $arrPostFixExp){
        global $operatorPrecedence;
        $stackOperands = new stack();
        foreach ($arrPostFixExp as $symItem){
            if (is_numeric($symItem))
                $stackOperands->pushItem($symItem);
            else{
                switch($symItem){
                    case $operatorPrecedence[DOT_OPT_MINUS]['name'] :
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        $anyResult = $firstOperand * (-1);
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_PLUS]['name'] :
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        $anyResult = $firstOperand * (+1);
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_POWER]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = pow($firstOperand,$secondOperand);
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_NOT]['name'] :
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_numeric($firstOperand))
                            $anyResult  = ~$firstOperand;
                        else
                            $anyResult = !$firstOperand;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_ADD]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = $firstOperand + $secondOperand;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_SUB]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = $firstOperand - $secondOperand;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_MUL]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = $firstOperand * $secondOperand;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_REAL_DIV]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        if ($secondOperand != 0)
                            $anyResult = $firstOperand / $secondOperand;
                        else
                            $anyResult = 0;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_INT_DIV]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        if ($secondOperand != 0){
                            $anyResult = $firstOperand / $secondOperand;
                            $anyResult = ($anyResult>=0)?floor($anyResult):ceil($anyResult);
                        }
                        else
                            $anyResult = 0;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_MOD]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        if ($secondOperand != 0)
                            $anyResult = $firstOperand % $secondOperand;
                        else
                            $anyResult = 0;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_AND]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        if (is_numeric($firstOperand))
                            $anyResult  = $firstOperand & $secondOperand;
                        else
                            $anyResult = $firstOperand && $secondOperand;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_OR]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        if (is_numeric($firstOperand))
                            $anyResult  = $firstOperand | $secondOperand;
                        else
                            $anyResult = $firstOperand || $secondOperand;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_LESS]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = ($firstOperand < $secondOperand)?TRUE:FALSE;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_LESS_EQUAL]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = ($firstOperand <= $secondOperand)?TRUE:FALSE;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_EQUAL]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = ($firstOperand == $secondOperand)?TRUE:FALSE;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_NOT_EQUAL]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = ($firstOperand != $secondOperand)?TRUE:FALSE;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_MORE_EQUAL]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = ($firstOperand >= $secondOperand)?TRUE:FALSE;
                        $stackOperands->pushItem($anyResult);
                        break;
                    case $operatorPrecedence[DOT_OPT_MORE]['name'] :
                        $secondOperand = $stackOperands->popItem();
                        $firstOperand = $stackOperands->popItem();
                        if (is_null($firstOperand)) return NULL;
                        if (is_null($secondOperand)) return NULL;
                        $anyResult = ($firstOperand > $secondOperand)?TRUE:FALSE;
                        $stackOperands->pushItem($anyResult);
                        break;
                    default:
                        return NULL;
                }
            }
        }
        if ($stackOperands->countItems()>1) return NULL;
        return $stackOperands->popItem();
    }
}
