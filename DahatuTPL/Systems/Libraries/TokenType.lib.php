<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/27/14
 * Time: 9:51 PM
 */

define('DTT_NO_TYPE',           0);
define('DTT_HTML_TAG',          1);
define('DTT_COMMENT',           2);
define('DTT_VARIABLE',          3);
define('DTT_LIT_STRING',        4);
define('DTT_LIT_NUMBER',        5);
define('DTT_EXPRESSION',        6);
define('DTT_STMT_IF',           7);
define('DTT_STMT_ELSE',         8);
define('DTT_STMT_ELSE_IF',      9);
define('DTT_STMT_END_IF',       10);
define('DTT_STMT_FOR',          11);
define('DTT_STMT_END_FOR',      12);
define('DTT_STMT_LOAD',         13);
define('DTT_STMT_LOOP',         14);
define('DTT_STMT_END_LOOP',     15);
define('DTT_ERROR',             16);
define('DTT_STMTS',             17);

function getTokenTypeByName($intType){
    $strType = '';
    switch($intType){
        case DTT_NO_TYPE:
            $strType = 'DTT_NO_TYPE';
            break;
        case DTT_HTML_TAG:
            $strType = 'DTT_HTML_TAG';
            break;
        case DTT_COMMENT:
            $strType = 'DTT_COMMENT';
            break;
        case DTT_VARIABLE:
            $strType = 'DTT_VARIABLE';
            break;
        case DTT_ERROR:
            $strType = 'DTT_ERROR';
            break;
        case DTT_LIT_STRING:
            $strType = 'DTT_LIT_STRING';
            break;
        case DTT_LIT_NUMBER:
            $strType = 'DTT_LIT_NUMBER';
            break;
        case DTT_EXPRESSION:
            $strType = 'DTT_EXPRESSION';
            break;
        case DTT_STMT_IF:
            $strType = 'DTT_STMT_IF';
            break;
        case DTT_STMT_ELSE:
            $strType = 'DTT_STMT_ELSE';
            break;
        case DTT_STMT_ELSE_IF:
            $strType = 'DTT_STMT_ELSE_IF';
            break;
        case DTT_STMT_END_IF:
            $strType = 'DTT_STMT_END_IF';
            break;
        case DTT_STMT_FOR:
            $strType = 'DTT_STMT_FOR';
            break;
        case DTT_STMT_END_FOR:
            $strType = 'DTT_STMT_END_FOR';
            break;
        case DTT_STMT_LOAD:
            $strType = 'DTT_STMT_LOAD';
            break;
        case DTT_STMT_LOOP:
            $strType = 'DTT_STMT_LOOP';
            break;
        case DTT_STMT_END_LOOP:
            $strType = 'DTT_STMT_END_LOOP';
            break;
        case DTT_STMTS:
            $strType = 'DTT_STMTS';
            break;
        default:
            $strType = 'DTT_NO_TYPE';
    }
    return $strType;
}
