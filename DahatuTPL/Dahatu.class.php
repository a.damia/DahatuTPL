<?php
/**
 * Created by PhpStorm.
 * User: EbdulRahman
 * Date: 4/27/14
 * Time: 6:33 PM
 */

require_once ("Systems/Config/Config.class.php");
require_once ("Systems/Interpreter.class.php");

final class Dahatu {
    private $strIndexTPLFileName=NULL;

    public function Dahatu($tplFilesPath, $indexTPLFile=''){
        Configuration::setDahatuRoot(__DIR__);
        Configuration::setTPLFilesPath($tplFilesPath);
        $this->setIndexTPLFile($indexTPLFile);
    }
    public function setIndexTPLFile($indexTPLFile){
        $this->strIndexTPLFileName = $indexTPLFile;
    }
    public function assignVariable($strVarName, $anyVarValue){
        VariableStory::setVariable($strVarName, $anyVarValue);
    }
    public function assignJSONValue($jsonValue, $jsonName=''){
        VariableStory::setValueFromJSON($jsonValue, $jsonName);
    }
    public function assignFromJSONFile($strFileName, $strPerfix = ''){
        if (file_exists($strFileName)){
            $strJSON = file_get_contents($strFileName);
            $this->assignJSONValue($strJSON, $strPerfix);
        }
    }
    public function assignArray(array $arrValues, $strPerfix = ''){
        foreach($arrValues as $vKey=>$vVal)
            VariableStory::setVariable($strPerfix.$vKey, $vKey);
    }
    public function evalSourceCodes($strTPLSourceCode){
        $interpretCodes = new Interpreter($strTPLSourceCode);
        return $interpretCodes->interpretOut();
    }
    public function displayOut(){
        $interPreter = new Interpreter($this->strIndexTPLFileName);
        return $interPreter->interpretOut();
    }
    public function saveConfigs($strPath=''){}
    public function loadConfigs($strPath=''){}
}